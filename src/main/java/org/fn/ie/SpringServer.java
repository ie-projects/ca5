package org.fn.ie;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class SpringServer {
    @RequestMapping(value = "/restaurant", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<Restaurant> getRestaurants()
    {
        return Nikivery.getInstance().getRestaurants();
    }

    @RequestMapping(value = "/food_party", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<PartyRestaurant> getPartyFoodReses()
    {
        return Nikivery.getInstance().getPartyFoodReses();
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserProfile getUserProfile()
    {
        return Nikivery.getInstance().getUserProfile();
    }

    @RequestMapping(value = "/cart", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<Order> getCart()
    {
        return Nikivery.getInstance().getUserProfile().getCart();
    }

    @RequestMapping(value = "/history", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<OrderStatus> getOrderStatuses()
    {
        return Nikivery.getInstance().getUserProfile().getDeliveryStates();
    }

    @RequestMapping(value = "/food_party_clock", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public long getFoodPartyClock()
    {
        int partyTime = 15 * 60 - (int) (Nikivery.getInstance().getFoodPartyClock() / 1000);
        return partyTime > 0 ? partyTime: 0;
    }

    @RequestMapping(value = "/receipt/{recNum}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Factor getFactor(@PathVariable(value = "recNum") int recNum)
    {
        return Nikivery.getInstance().getUserProfile().getFactor(recNum);
    }

    @RequestMapping(value = "/restaurant/{resid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Restaurant getRestaurants(@PathVariable(value = "resid") String resid)
    {
        return Nikivery.getInstance().getRestaurant(resid);
    }

    @RequestMapping(value = "/order", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public int addToCart(
            @RequestParam(value = "foodName") String foodName,
            @RequestParam(value = "restaurantId") String restaurantId,
            @RequestParam(value = "isParty") boolean isParty)
    {
        return Nikivery.getInstance().addToCart(foodName, restaurantId, isParty);
    }

    @RequestMapping(value = "/plus", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public int incrementInCart(
            @RequestParam(value = "foodName") String foodName)
    {
        return Nikivery.getInstance().incrementInCart(foodName);
    }

    @RequestMapping(value = "/minus", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public int decrementInCart(
            @RequestParam(value = "foodName") String foodName)
    {
        return Nikivery.getInstance().decrementInCart(foodName);
    }

    @RequestMapping(value = "/recharge", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public int addCredit(
            @RequestParam(value = "upCredit") int upCredit)
    {
        return Nikivery.getInstance().getUserProfile().addCredit(upCredit);
    }

    @RequestMapping(value = "/finalize", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public int finalizeOrder()
    {
        return Nikivery.getInstance().finalizeOrder();
    }
}
