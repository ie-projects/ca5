package org.fn.ie;

public class Food {
    private String name;
    private String description;
    private float popularity;
    private int price;
    private String image;
    private String restaurantName;
    private boolean isParty;

    public Food()
    {
        isParty = false;
    }

    public Food(Food former) {
        this.name = former.name;
        this.description = former.description;
        this.popularity = former.popularity;
        this.price = former.price;
        this.image = former.image;
        this.restaurantName = former.restaurantName;
        this.isParty = former.isParty;
    }

    public boolean isParty() {
        return isParty;
    }

    public void copyFood(PartyFood partyFood)
    {
        this.name = partyFood.getName();
        this.description = partyFood.getDescription();
        this.popularity = partyFood.getPopularity();
        this.price = partyFood.getPrice();
        this.image = partyFood.getImage();
        isParty = true;
    }

    public String getName() { return name; }

    public String getDescription() { return description; }

    public float getPopularity() { return popularity; }

    public String getRestaurantName() { return restaurantName; }

    public int getPrice() { return price; }

    public String getImage() { return image; }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPopularity(float popularity) {
        this.popularity = popularity;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }
}
