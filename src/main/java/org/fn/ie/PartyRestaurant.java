package org.fn.ie;

import java.util.ArrayList;

public class PartyRestaurant {
    private String id;
    private String name;
    private Location location;
    private String logo;
    private ArrayList<PartyFood> menu;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public ArrayList<PartyFood> getMenu() {
        return menu;
    }

    public void setMenu(ArrayList<PartyFood> menu) {
        this.menu = menu;
    }
}
