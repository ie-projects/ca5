package org.fn.ie;

import java.util.ArrayList;

public class UserProfile {
    private int id;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private int credit;
    private ArrayList<Food> cart;
    private ArrayList<Factor> factors;
    private String cartRestaurant;
    private ArrayList<OrderStatus> deliveryStates;

    public UserProfile(int _id, String fname, String lname, String pnumber, String mail, int cred)
    {
        id = _id;
        firstName = fname;
        lastName = lname;
        phoneNumber = pnumber;
        email = mail;
        credit = cred;
        cart = new ArrayList<Food>();
        factors = new ArrayList<Factor>();
        deliveryStates = new ArrayList<OrderStatus>();
    }

    public int getId() { return id; }
    public String getFirstName() { return firstName; }
    public String getLastName() { return lastName; }
    public String getPhoneNumber() { return phoneNumber; }
    public String getEmail() { return email; }
    public int getCredit() { return credit; }
    public String getCartRestaurant() { return cartRestaurant; }
    public int addCredit(int up) { credit += up; return credit; }
    public void addFoodToCart(Food food) { cart.add(food); }
    public void setCartRestaurant(String cartRestaurant) { this.cartRestaurant = cartRestaurant; }
    public void setDeliveryState(int deliveryState, int i)
    {
        if(i == -1) {
            for (int j = 0; j < Nikivery.getInstance().getRestaurants().size(); j++)
                if(Nikivery.getInstance().getRestaurants().get(j).getId().equals(this.getCartRestaurant()))
                    deliveryStates.add(new OrderStatus(deliveryState, Nikivery.getInstance().getRestaurants().get(j).getName()));
        }
        else if(i < deliveryStates.size())
            deliveryStates.get(i).setStatus(deliveryState);
    }

    private ArrayList<Order> organizeOrders()
    {
        ArrayList<Order> orders = new ArrayList<Order>();
        for(int i = 0; i < cart.size(); i++)
        {
            int foodCount = 1;
            boolean repetitive = false;
            for(int j =0; j < cart.size(); j++)
                if(cart.get(i).getName().equals(cart.get(j).getName()) && cart.get(i).getPrice() == cart.get(j).getPrice())
                {
                    if(i == j)
                        continue;
                    if(j < i)
                    {
                        repetitive = true;
                        break;
                    }
                    foodCount++;
                }
            if(!repetitive)
                orders.add(new Order(cart.get(i).getName(), foodCount, cart.get(i).getPrice()));
        }
        return orders;
    }
    public ArrayList<Order> getCart()
    {
        ArrayList<Order> orders = organizeOrders();
        return orders;
    }

    public ArrayList<Food> getRawCart()
    {
        return this.cart;
    }

    public void emptyCart()
    {
        cart = new ArrayList<Food>();
    }

    public ArrayList<Factor> getFactors() {
        return factors;
    }

    public Factor getFactor(int i) {
        if(i < factors.size() && i >= 0)
            return factors.get(i);
        else
            return null;
    }

    public ArrayList<OrderStatus> getDeliveryStates() {
        return deliveryStates;
    }

    public void addToFactors(String restaurantName, ArrayList<Order> orders, int netPrice)
    {
        ArrayList<Restaurant> restaurants = Nikivery.getInstance().getRestaurants();
        for (int i = 0; i < restaurants.size(); i++)
            if (restaurants.get(i).getId().equals(restaurantName))
            {
                factors.add(new Factor(restaurants.get(i).getName(), orders, netPrice));
                return;
            }
    }

    public void removeFoodFromCart(int i)
    {
        if(i < cart.size() && i >= 0)
            cart.remove(i);
        else
            return;
    }
}
